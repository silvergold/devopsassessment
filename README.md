# DevopsAssessment

Silver and Alex

Silver worked on: MySQL image creation and MySQL yaml lines
Alex worked on: Apache image creation and Apache yaml lines
We both worked on: merging the branches together, resolving conflicts and then the final running of the docker containers

Before building, please run the command sudo rm -rf mydb/ to avoid any permission errors regarding the mydb folder.

The docker command to build the container is: docker-compose build
The docker command to run the containers is: docker-compose up

We have completed the bonus task - Silver lives in China and Alex lives in Canada! :) 
